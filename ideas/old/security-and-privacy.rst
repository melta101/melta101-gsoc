.. _gsoc-idea-security-and-privacy:

Security and privacy
#####################

.. note:: This project section requires updates, feel free to suggest new projects of your own.

The BeagleBoard.org community cares a lot about understanding how information is managed. 
Projects that open up technology for more personal control and help secure data from external 
manipulation are generally well accepted. Consider how you can make contributions to existing projects in this area.

- penetration testing (The Deck, Kali)
- security appliance (Pangolin, ZeroTier Edge device)
- self-hosted services (Freedombone)
- car hacking (Macchina)

Example idea, a portable/plugable security appliance for use anywhere:

- hardware: beaglebone with ethernet or beaglebone wireless, pocketbone + USB ethernet/wifi
- OS: openwrt
- configuration UI: bonescript/other
- PRU accelerators...
- privacy and safety tools...