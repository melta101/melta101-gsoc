.. _gsoc-guides:

Guides
######

.. youtube:: 7jD2tChhrWM
   :width: 100%
   :align: center

.. admonition:: What is Google Summer of Code?
   :class: admonition-code

   Spend your summer break writing code and learning about open source development while earning money!
   Accepted contributors work with a mentor and become a part of the open source community. Many become lifetime
   open source developers! The 2024 contributor application window will be open from
   `March 18th 2024 <https://developers.google.com/open-source/gsoc/timeline#march_18_-_1800_utc>`_ to
   `April 2nd 2024 <https://developers.google.com/open-source/gsoc/timeline#april_2_-_1800_utc>`_!
   But don't wait for then to engage! Come to our `Discord <https://bbb.io/gsocchat>`_ and
   `Forum <https://bbb.io/gsocml>`_ to share ideas today.

This section includes guides for :ref:`contributors <gsoc-contributor-guide>` & :ref:`mentors <gsoc-mentor-guide>` who want to participate 
in GSoC 2024 with `BeagleBoard.org <www.beagleboard.org>`_. It's highly recommended to check `GSoC Frequently Asked Questions 
<https://developers.google.com/open-source/gsoc/faq>`_. For anyone who just want to contribute to this site we also have 
a step by step :ref:`contribution guide <gsoc-site-editing-guide>`.

.. card::
    :link: gsoc-contributor-guide
    :link-type: ref

    :fas:`graduation-cap` **Contributor Guide**
    ^^^^

    If you want to increase your chances of being accepted into (and succeed during) Google Summer of Code, we strongly 
    encourage you to read this guide! It is particularly helpful for tips on writing a good project proposal and how 
    potential contributors should interact with the communities they are interested in working with during the program.

.. card:: 
    :link: gsoc-mentor-guide
    :link-type: ref 
        
    :fas:`person-chalkboard` **Mentor Guide**
    ^^^^

    If you want to mentor for GSoC this year, read this short guide made for mentors wondering where to help.

.. card:: 
    :link: gsoc-proposal-guide
    :link-type: ref 
        
    :fas:`file` **Proposal Guide**
    ^^^^

    To create an proposal on ``gsoc.beagleboard.io``, we have provided a reStructuredText template. The tamplate can be used 
    to create a live HTML version of the proposal as well as generate a PDF version for GSoC submission.

.. card:: 
    :link: gsoc-site-editing-guide
    :link-type: ref 
        
    :fas:`file-signature` **Site Editing Guide**
    ^^^^

    For anyone who is looking to make changes to the ``gsoc.beagleboard.io`` site, you can checkout our site editing guide. We have 
    provided information on how to use OpenBeagle's integrated ``Web IDE`` for editing, ``CI`` for building and ``pages`` for 
    serving the rendered content.



.. toctree:: 
    :hidden:
    :maxdepth: 2

    contributor
    mentor
    proposal
    site-editing
